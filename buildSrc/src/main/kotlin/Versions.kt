object Versions {
    const val BOUNCY_CASTLE = "1.70"
    const val CAFFEINE = "3.1.8"
    const val CBOR = "0.4.0-NEWM"
    const val CHECKER_FRAMEWORK = "3.48.3"
    const val COROUTINES = "1.10.1"
    const val ERROR_PRONE = "2.36.0"
    const val EXPOSED = "0.57.0"
    const val GOOGLE_TRUTH = "1.4.4"
    const val HIKARI = "6.2.1"
    const val JACKSON = "2.18.2"
    const val JODA_TIME = "2.13.0"
    const val JSON = "20240303"
    const val JSOUP = "1.18.3"
    const val JUNIT = "5.11.4"
    const val KOTLIN = "2.1.0"
    const val KOTLIN_LOGGING = "7.0.3"
    const val KSP = "2.1.0-1.0.29"
    const val KTOR = "3.0.3"
    const val LIBSODIUM_JNA = "1.2.0-NEWM"
    const val LIQUIBASE = "4.30.0"
    const val MOCKK = "1.13.14"
    const val MOSHI = "1.15.2"
    const val OKHTTP = "4.12.0"
    const val POSTGRESQL = "42.7.4"
    const val RETROFIT = "2.11.0"
    const val SPRING_BOOT = "3.4.1"
    const val SPRING_DEPENDENCY = "1.1.7"
    const val SPRING_SECURITY = "6.4.2"
    const val SSHJ = "0.39.0"
    const val VERSIONS = "0.51.0"
}
