package com.firehose.controllers.nodeclient.protocol

enum class Agency {
    Client,
    Server,
    None,
}