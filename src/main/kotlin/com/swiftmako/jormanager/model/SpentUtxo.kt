package com.swiftmako.jormanager.model

data class SpentUtxo(
    val hash: String,
    val ix: Long,
)