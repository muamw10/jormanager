package com.swiftmako.jormanager.model

import com.fasterxml.jackson.annotation.JsonProperty
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class FileUpload(
        @JsonProperty("name") @Json(name = "name") val name: String,
        @JsonProperty("content") @Json(name = "content") val content: String
)