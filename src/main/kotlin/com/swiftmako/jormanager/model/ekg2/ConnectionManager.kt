package com.swiftmako.jormanager.model.ekg2


import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class ConnectionManager(
    @Json(name = "duplexConns")
    val duplexConns: IntX = IntX(),
    @Json(name = "incomingConns")
    val incomingConns: IntX = IntX(),
    @Json(name = "outgoingConns")
    val outgoingConns: IntX = IntX(),
    @Json(name = "prunableConns")
    val prunableConns: IntX = IntX(),
    @Json(name = "unidirectionalConns")
    val unidirectionalConns: IntX = IntX(),
)