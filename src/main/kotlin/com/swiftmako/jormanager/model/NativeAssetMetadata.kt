package com.swiftmako.jormanager.model

data class NativeAssetMetadata(
    val assetName: String,
    val assetPolicy: String,
    val metadataName: String,
    val metadataImage: String,
    val metadataDescription: String?
)