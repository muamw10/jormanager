# JorManager

JorManager is a GUI-based Cardano stakepool management system. 

## What can it do?

### Monitor nodes and keys on a pretty dashboard
![Dashboard](images/dashboard.png)

### Create connections to remote hosts
![Hosts](images/hosts.png)

### Restart nodes, Rotate KES Keys, Modify pool fees, Edit pool metadata 
![Nodes](images/nodes.png)

### Create nodes
![Create Node](images/create_node.png)

### Monitor and Validate blocks. Calculate upcoming blocks (leader logs).
![Blocks](images/blocks.png)

### Manage Stakepool funds, claim rewards, perform complex transactions.
![Wallet](images/wallet.png)
![Send Funds](images/send_funds.png)
![Create Wallet Entry](images/create_wallet_entry.png)

## Get Started

Head over to our Wiki site for how to install and configure JorManager.

[JorManager Wiki Home](https://bitbucket.org/muamw10/jormanager/wiki/Home)

## Support

If you need support, the ask for help on this telegram channel -> https://t.me/jormanager

#### Project Support

This project is designed to do nothing more than help out the Cardano community and ecosystem. It's offered without charge and **without warranty** of any kind. However, if you feel inclined to tip the developer, that can be done by sending MainNet ADA to the following address:
  
```
addr1q8044ycsxth7gdfcp3uqus3r7y33agkqxy0gygq2xlarp08l27sthj42mfetdc7kmyzycssdr2xajau53pxnjqslr63sntagm2
```